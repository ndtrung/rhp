/*
  Estimate the protein surface coverage by random copolymers
    polymer beads are of types 1, 2, 3 and 4
    protein beads are of types 5 and 6

  Build:
    g++ -O2 -o find_coverage surface_coverage.cpp

  Use:
    ./find_coverage snapshot-fA-0.5-epp-0.8-ehp-1.5.dump 2.0

    where snapshot-fA-0.5-epp-0.8-ehp-1.5.dump is a LAMMPS dump file
          2.0 is the thickness of the spherical shell next to the protein surface to bin the polymer beads

  Author: Trung Nguyen (Northwestern), 10/2016
*/

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <cmath>
#include <string.h>

#define anint(x) ((x >= 0.5) ? (1.0) : (x < -0.5) ? (-1.0) : (0.0))	
#define MAX_NEIGHBORS 100
#define EPSILON 0.0001

using namespace std;

struct Bead
{
  int id;
  int mol;
  int type;
  double q;
  double x;
  double y;
  double z;
};

typedef vector<Bead> BeadList;

class Vec3
{
  friend Vec3 operator + (const Vec3& v1, const Vec3& v2) {
    return Vec3(v1.x+v2.x, v1.y+v2.y, v1.z+v2.z);
  }

  friend Vec3 operator - (const Vec3& v1, const Vec3& v2) {
    return Vec3(v1.x-v2.x, v1.y-v2.y, v1.z-v2.z);
  }

  friend Vec3 operator * (const Vec3& v, float k) {
    return Vec3(k*v.x, k*v.y, k*v.z);
  }

public:
  Vec3() : added(0) {}

  Vec3(float _x, float _y, float _z) : x(_x), y(_y), z(_z), added(0) {}

  Vec3(const Vec3& v) { 
    x = v.x;
    y = v.y;
    z = v.z;
    added = v.added;
  }

  Vec3& operator = (const Vec3& v) {
    if (&v == this) return *this;
    x = v.x;
    y = v.y;
    z = v.z;
    added = v.added;
    return *this;
  }

  float length() const {
    return sqrt(x*x+y*y+z*z);
  }

  Vec3 normalize() {
    float r = sqrt(x*x+y*y+z*z);
    return Vec3(x/r, y/r, z/r);
  }

  float x, y, z;
  int added;
};

class GeodegicSphere
{
public:
  Vec3 center;
  float radius;
  float maxEdgeLength;

  // "kitchen sink" constructor (allows specifying everything)
  GeodegicSphere(const Vec3 _center, const float _radius, const float _maxEdgeLength)
    : center (_center), radius (_radius), maxEdgeLength (_maxEdgeLength) {}

  // draw as an icosahedral geodesic sphere
  int triangulate (double**& coords) const {
    const float sqrt5 = sqrt(5.0f);
    const float phi = (1.0f + sqrt5) * 0.5f; // "golden ratio"

    // ratio of edge length to radius
    const float ratio = sqrt (10.0f + (2.0f * sqrt5)) / (4.0f * phi);
    const float a = (radius / ratio) * 0.5;
    const float b = (radius / ratio) / (2.0f * phi);

    std::vector<Vec3> list;

    int ntriangles = 0;

    // define the icosahedron's 12 vertices:
    Vec3 v1  = center + Vec3( 0,  b, -a);
    Vec3 v2  = center + Vec3( b,  a,  0);
    Vec3 v3  = center + Vec3(-b,  a,  0);
    Vec3 v4  = center + Vec3( 0,  b,  a);
    Vec3 v5  = center + Vec3( 0, -b,  a);
    Vec3 v6  = center + Vec3(-a,  0,  b);
    Vec3 v7  = center + Vec3( 0, -b, -a);
    Vec3 v8  = center + Vec3( a,  0, -b);
    Vec3 v9  = center + Vec3( a,  0,  b);
    Vec3 v10 = center + Vec3(-a,  0, -b);
    Vec3 v11 = center + Vec3( b, -a,  0);
    Vec3 v12 = center + Vec3(-b, -a,  0);

    // draw the icosahedron's 20 triangular faces:
    drawMeshedTriangleOnSphere(v1, v2, v3, list, ntriangles);
    drawMeshedTriangleOnSphere(v4, v3, v2, list, ntriangles);
    drawMeshedTriangleOnSphere(v4, v5, v6, list, ntriangles);
    drawMeshedTriangleOnSphere(v4, v9, v5, list, ntriangles);
    drawMeshedTriangleOnSphere(v1, v7, v8, list, ntriangles);
    drawMeshedTriangleOnSphere(v1, v10, v7, list, ntriangles);
    drawMeshedTriangleOnSphere(v5, v11, v12, list, ntriangles);
    drawMeshedTriangleOnSphere(v7, v12, v11, list, ntriangles);
    drawMeshedTriangleOnSphere(v3, v6, v10, list, ntriangles);
    drawMeshedTriangleOnSphere(v12, v10, v6, list, ntriangles);
    drawMeshedTriangleOnSphere(v2, v8, v9, list, ntriangles);
    drawMeshedTriangleOnSphere(v11, v9, v8, list, ntriangles);
    drawMeshedTriangleOnSphere(v4, v6, v3, list, ntriangles);
    drawMeshedTriangleOnSphere(v4, v2, v9, list, ntriangles);
    drawMeshedTriangleOnSphere(v1, v3, v10, list, ntriangles);
    drawMeshedTriangleOnSphere(v1, v8, v2, list, ntriangles);
    drawMeshedTriangleOnSphere(v7, v10, v12, list, ntriangles);
    drawMeshedTriangleOnSphere(v7, v11, v8, list, ntriangles);
    drawMeshedTriangleOnSphere(v5, v12, v6, list, ntriangles);
    drawMeshedTriangleOnSphere(v5, v9, v11, list, ntriangles);

    // remove the overlapping vertices
    double xtmp, ytmp, ztmp, dx, dy, dz, rsq;
    for (int i = 0; i < list.size()-1; i++) {
      xtmp = list[i].x;
      ytmp = list[i].y;
      ztmp = list[i].z;
      for (int j = i+1; j < list.size(); j++) {
        dx = xtmp - list[j].x;
        dy = ytmp - list[j].y;
        dz = ztmp - list[j].z;
        rsq = dx * dx + dy * dy + dz * dz;
        if (rsq <= 0.1) list[j].added = 0;          
      }
    }

    int n = 0;
    for (int i = 0; i < list.size(); i++)
      if (list[i].added == 1) n++;

    coords = new double* [n];
    for (int i = 0; i < n; i++)
      coords[i] = new double [3];

    int m = 0;
    for (int i = 0; i < list.size(); i++) {
      if (list[i].added == 1) {
        coords[m][0] = list[i].x;
        coords[m][1] = list[i].y;
        coords[m][2] = list[i].z;
        m++;
      }
    }

    // dump file
/*
    FILE* fp = fopen("bincoords.txt", "w");

    float xlo = 0;
    float xhi = 2*center.x;
    float ylo = 0;
    float yhi = 2*center.y;
    float zlo = 0;
    float zhi = 2*center.z;

    fprintf(fp, "ITEM: TIMESTEP\n0\n");
    fprintf(fp, "ITEM: NUMBER OF ATOMS\n%d\n", n);
    fprintf(fp, "ITEM: BOX BOUNDS pp pp pp\n");
    fprintf(fp, "%f %f\n", xlo, xhi);
    fprintf(fp, "%f %f\n", ylo, yhi);
    fprintf(fp, "%f %f\n", zlo, zhi);
    fprintf(fp, "ITEM: ATOMS id type q x y z\n");

    int id = 1;
    for (int i = 0; i < list.size(); i++) {
      if (list[i].added == 1) {
        fprintf(fp, "%d 1 0 %g %g %g\n", id, list[i].x, list[i].y, list[i].z);
        id++;
      }
    }

    fclose(fp);
*/
    return n;
  }

  // given two points, take midpoint and project onto this sphere
  inline Vec3 midpointOnSphere(const Vec3& a, const Vec3& b) const {
    const Vec3 midpoint = (a + b) * 0.5f;
    const Vec3 unitRadial = (midpoint - center).normalize();
    return center + (unitRadial * radius);
  }

  // given three points on the surface of this sphere, if the triangle
  // is "small enough" draw it, otherwise subdivide it into four smaller
  // triangles and recursively draw each of them.
  void drawMeshedTriangleOnSphere(Vec3& a,
                                  Vec3& b,
                                  Vec3& c,
                                  std::vector<Vec3>& list, int& ntriangles) const  {
    // if all edges are short enough
    if ((((a - b).length()) <= maxEdgeLength) &&
        (((b - c).length()) <= maxEdgeLength) &&
        (((c - a).length()) <= maxEdgeLength)) {

      if (a.added == 0) {
        a.added = 1;
        list.push_back(a);
      }
      if (b.added == 0) {
        b.added = 1;
        list.push_back(b);

      }
      if (c.added == 0) {
        c.added = 1;
        list.push_back(c);
      }

      ntriangles++;

    } else { // otherwise subdivide and recurse
      // find edge midpoints
      Vec3 ab = midpointOnSphere(a, b);
      Vec3 bc = midpointOnSphere(b, c);
      Vec3 ca = midpointOnSphere(c, a);
      // recurse on four sub-triangles
      drawMeshedTriangleOnSphere( a, ab, ca, list, ntriangles);
      drawMeshedTriangleOnSphere(ab,  b, bc, list, ntriangles);
      drawMeshedTriangleOnSphere(ca, bc,  c, list, ntriangles);
      drawMeshedTriangleOnSphere(ab, bc, ca, list, ntriangles);
    }
  }
 
};

void read_dump(char* fileName, int& nNumTotalBeads,
               double& Lx, double& Ly, double& Lz,
               vector<Bead>& beadList)
{
	int i;
  char* line = new char[1024];
  double xlo,xhi,ylo,yhi,zlo,zhi;
	Bead bead;
  
	ifstream ifs(fileName);
  for (i = 0; i < 9; i++) {
  	ifs.getline(line, 1024);
    if (i==3) sscanf(line, "%d", &nNumTotalBeads);
    if (i==5) sscanf(line, "%lf %lf", &xlo, &xhi);
    if (i==6) sscanf(line, "%lf %lf", &ylo, &yhi);
    if (i==7) sscanf(line, "%lf %lf", &zlo, &zhi);
  }
  Lx = xhi - xlo;
  Ly = yhi - ylo;
  Lz = zhi - zlo;

  for (i = 0; i < nNumTotalBeads; i++) {
		ifs.getline(line, 1024);
    sscanf(line, "%d %d %d %lf %lf %lf %lf",
      &bead.id, &bead.mol, &bead.type, &bead.q, &bead.x, &bead.y, &bead.z);
  	beadList.push_back(bead);
	}
  ifs.close();

  delete [] line;
}

void write_dump(const BeadList& beadList, double Lx, double Ly, double Lz, char* filename)
{
  int nNumBeads = beadList.size();
  ofstream ofs(filename);
  ofs << "ITEM: TIMESTEP\n0\n";
  ofs << "ITEM: NUMBER OF ATOMS\n";
  ofs << nNumBeads << "\n";
  ofs << "ITEM: BOX BOUNDS pp pp pp\n";
  ofs << "0 " << Lx << "\n";
  ofs << "0 " << Ly << "\n";
  ofs << "0 " << Lz << "\n";
  ofs << "ITEM: ATOMS id mol type q x y z\n";

  for (int i = 0; i < nNumBeads; i++) {
    ofs << beadList[i].id << " ";
    ofs << beadList[i].mol << " ";
    ofs << beadList[i].type << " ";
    ofs << beadList[i].q << " ";
    ofs << beadList[i].x << " ";
    ofs << beadList[i].y << " ";
    ofs << beadList[i].z << "\n";
  }

  ofs.close();
}

// select the beads in the input of the specified types
void get_beads(const BeadList& input, int* types, int ntypes, BeadList& output)
{
  int i, m, nNumBeads;
  nNumBeads = input.size();
  
  output.clear();
  
  for (i = 0; i < nNumBeads; i++) {
    int included = 0;
    for (m = 0; m < ntypes; m++) {
      if (input[i].type == types[m]) {
        included = 1;
        break;
      }
    }
    if (included == 0) continue;

    output.push_back(input[i]);
  }
}

// Gyration tensor, radius of gyration and center of mass

void rotate(double **matrix, int i, int j, int k, int l, double s, double tau)
{
	double g = matrix[i][j];
	double h = matrix[k][l];
	matrix[i][j] = g - s * (h + g * tau);
	matrix[k][l] = h + s * (g - h * tau);
} 

#define MAXJACOBI 50
int diagonalize(double **matrix, double *evalues, double **evectors)
{
	int i, j, k, iter;
	double tresh, theta, tau, t, sm, s, h, g, c, b[3], z[3];
	
	for (i = 0; i < 3; i++) {
		for (j = 0; j < 3; j++) 
			evectors[i][j] = 0.0;
		
		evectors[i][i] = 1.0;
	}
	
	for (i = 0; i < 3; i++)	{
		b[i] = evalues[i] = matrix[i][i];
		z[i] = 0.0;
	}
	
	for (iter = 1; iter <= MAXJACOBI; iter++) {
		sm = 0.0;
		for (i = 0; i < 2; i++)
			for (j = i+1; j < 3; j++)
				sm += fabs(matrix[i][j]);
			if (sm == 0.0) 
				return 0;
			
		if (iter < 4) 
			tresh = 0.2 * sm / (3 * 3);
		else 
			tresh = 0.0;
			
		for (i = 0; i < 2; i++) {
			for (j = i+1; j < 3; j++) {
				g = 100.0*fabs(matrix[i][j]);
				if (iter > 4 && fabs(evalues[i])+g == fabs(evalues[i])
					&& fabs(evalues[j])+g == fabs(evalues[j]))
					matrix[i][j] = 0.0;
				else if (fabs(matrix[i][j]) > tresh) {
					h = evalues[j] - evalues[i];
					if (fabs(h)+g == fabs(h)) 
						t = (matrix[i][j]) / h;
					else {
						theta = 0.5 * h / (matrix[i][j]);
						t = 1.0 / (fabs(theta) + sqrt(1.0 + theta * theta));
						if (theta < 0.0) 
							t = -t;
					}

					c = 1.0 / sqrt(1.0 + t * t);
					s = t * c;
					tau = s / (1.0 + c);
					h = t * matrix[i][j];
					z[i] -= h;
					z[j] += h;
					evalues[i] -= h;
					evalues[j] += h;
					matrix[i][j] = 0.0;
					
					for (k = 0; k < i; k++)
						rotate(matrix, k, i, k, j, s, tau);
					
					for (k = i+1; k < j; k++) 
						rotate(matrix, i, k, k, j, s, tau);
					
					for (k = j+1; k < 3; k++) 
						rotate(matrix, i, k, j, k, s, tau);
					
					for (k = 0; k < 3; k++) 
						rotate(evectors, k, i, k, j, s, tau);
				}
			}
		}
			
		for (i = 0; i < 3; i++) {
			evalues[i] = b[i] += z[i];
			z[i] = 0.0;
		}
	}

	return 1;
}

// find the center of mass of a group of beads
// taking into account perioduc boundary conditions
void center_of_mass(BeadList& beadList, double Lx, double Ly, double Lz,
                    double* xm, int period_wrapped)
{
	int i, m, nNumBeads;
	double dx, dy, dz, Lx2, Ly2, Lz2;
	Bead pivot;

	nNumBeads = beadList.size();

	if (period_wrapped == 1) {
		Lx2 = Lx / 2.0;
		Ly2 = Ly / 2.0;
		Lz2 = Lz / 2.0;

		pivot.x = beadList[0].x;
		pivot.y = beadList[0].y;
		pivot.z = beadList[0].z;

		// Release periodic boundary conditions for all beads in cluster

		for (i = 1; i < nNumBeads; i++)	{

			dx = beadList[i].x - pivot.x;
			dy = beadList[i].y - pivot.y;
			dz = beadList[i].z - pivot.z;

			if (dx > Lx2)
				beadList[i].x -= Lx;
			else if (dx < -Lx2)
				beadList[i].x += Lx;

			if (dy > Ly2)
				beadList[i].y -= Ly;
			else if (dy < -Ly2)
				beadList[i].y += Ly;

			if (dz > Lz2)
				beadList[i].z -= Lz;
			else if (dz < -Lz2)
				beadList[i].z += Lz;
		}
	}

	// Compute the center of mass

	xm[0] = xm[1] = xm[2] = 0.0;
  int nCount = 0;
	for (i = 0; i < nNumBeads; i++) {
		xm[0] += beadList[i].x;
		xm[1] += beadList[i].y;
		xm[2] += beadList[i].z;
    nCount++;
	}

	xm[0] /= (double)nCount;
	xm[1] /= (double)nCount;
	xm[2] /= (double)nCount;
}

void gyration_tensor(BeadList& beadList, double Lx, double Ly, double Lz,
                     double* R2, double& Rg, double& maxrsq, double* xm)
{
	int i, j, k, m, nNumBeads;
	double **gyration;
	int periodic_wrapped = 0;

	gyration = new double* [3];
	for (i = 0; i < 3; i++)
		gyration[i] = new double [3];

	// Find the center of mass
	center_of_mass(beadList, Lx, Ly, Lz, xm, periodic_wrapped);

	for (j = 0; j < 3; j++)
		for (k = 0; k < 3; k++)
			gyration[j][k] = 0.0;
	
	double xi[3], L[3], rsq;
	L[0] = Lx; L[1] = Ly; L[2] = Lz;
  int nCount = 0;
  maxrsq = 0;

	nNumBeads = beadList.size();
	for (i = 0; i< nNumBeads; i++)	{

		xi[0] = beadList[i].x;
		xi[1] = beadList[i].y;
		xi[2] = beadList[i].z;
		for (j = 0; j < 3; j++)	{
			double rj = xi[j] - xm[j];
			if (rj > L[j]/2)
				rj -= L[j];
			else if (rj < -L[j]/2)
				rj += L[j];
							
			for (k = 0; k < 3; k++)	{
				double rk = xi[k] - xm[k];
				if (rk > L[k]/2)
					rk -= L[k];
				else if (rk < -L[k]/2)
					rk += L[k];
					
				gyration[j][k] += rj * rk; //(xi[j] - xm[j]) * (xi[k] - xm[k]);
			}
		}

    rsq = (xi[0] - xm[0])*(xi[0] - xm[0]) + (xi[1] - xm[1])*(xi[1] - xm[1])
      + (xi[2] - xm[2]) *(xi[2] - xm[2]);
    if (rsq > maxrsq) maxrsq = rsq;

    nCount++;
	}

	for (j = 0; j < 3; j++)
		for (k = 0; k < 3; k++)
			gyration[j][k] /= (double)nCount;

	// Dummy eigen_vectors
	double** eigen_vectors = new double* [3];
	for (i = 0; i < 3; i++)
		eigen_vectors[i] = new double [3];

	// Diagonalize the gyration tensor
  // the eigenvalues are squared radii of gyration
	diagonalize(gyration, R2, eigen_vectors);

  // radius of gyration
  Rg = sqrt(R2[0] + R2[1] + R2[2]);

	for (i = 0; i < 3; i++)
		delete [] eigen_vectors[i];
  delete [] eigen_vectors;

	for (i = 0; i < 3; i++)
		delete [] gyration[i];
	delete [] gyration;
}

// use same notation of phi and theta as in Baofu's report (commonly used in physics)
// phi = azimuthal angle, varying from 0 to 2pi
// theta = polar angle, varying from 0 to pi

void find_outer_layer(const BeadList& beadList, double* xm, int nbins, BeadList& outlist)
{
  int i, ix, iy, idx;
  double dx, dy, dz, rsq, rxy, theta, phi;
  int nNumBeads = beadList.size();
  double pi = 4.0*atan(1.0);

  double* bins = new double [2*nbins*nbins];
  int* indices = new int [2*nbins*nbins];
  double dphi = 360.0 / (2*nbins);
  double dtheta = 180.0 / nbins;
  for (i = 0; i < nbins*nbins; i++)
    bins[i] = indices[i] = 0;

  for (i = 0; i < nNumBeads; i++) {
    dx = beadList[i].x - xm[0];
    dy = beadList[i].y - xm[1];
    dz = beadList[i].z - xm[2];

    rsq = dx*dx + dy*dy + dz*dz;
    rxy = sqrt(dx*dx + dy*dy);
    theta = atan2(rxy, dz);
    theta = theta * 180.0 / pi;
    phi = atan2(dy, dx);
    if (phi < 0) phi += 2.0*pi;
    phi = phi * 180.0 / pi;

    ix = (int)((phi - 0) / dphi);
    iy = (int)((theta - 0) / dtheta);
    if (ix >= 0 && ix < 2*nbins && iy >= 0 && iy < nbins) {
      if (bins[ix*nbins+iy] < rsq) {
        bins[ix*nbins+iy] = rsq;
        indices[ix*nbins+iy] = i;
      }
    }
  }

  for (i = 0; i < nbins*nbins; i++) {
    if (bins[i] > 0) {
      idx = indices[i];
      outlist.push_back(beadList[idx]);
    }
  }

  delete [] bins;
  delete [] indices;
}

// find the subset of protein that is in contact with polymers within the cutoff distance
void find_contact_group(const BeadList& protein, const BeadList& polymers,
                        double cutoff, BeadList& surface)
{
  int i, j, num_polymers, num_protein;
  double xtmp, ytmp, ztmp, delx, dely, delz, rsq;
  double cutoff2 = cutoff * cutoff;

  num_polymers = polymers.size();
  num_protein = protein.size();

  for (i = 0; i < num_polymers; i++) {
    xtmp = polymers[i].x;
    ytmp = polymers[i].y;
    ztmp = polymers[i].z;
    for (j = 0; j < num_protein; j++) {
      delx = xtmp - protein[j].x;
      dely = ytmp - protein[j].y;
      delz = ztmp - protein[j].z;
      rsq = delx * delx + dely * dely + delz * delz;
      if (rsq < cutoff2) {
        // brute force checking for existing beads
        int found = 0;
        for (int m = 0; m < surface.size(); m++) {
          if (protein[j].id == surface[m].id) {
            found = 1;
            break;
          }
        }
        if (found == 0) surface.push_back(protein[j]);
      }
    }
  }
}

void density_profile(const BeadList& beadList, double* xm, double dr, double rmax, const char* filename)
{
  int i, m, ibin, nbins;
  double dx, dy, dz, r;
  int nNumBeads = beadList.size();
  double pi = 4.0*atan(1.0);

  nbins = (int)(rmax / dr);
  int* bins = new int [nbins];
  for (i = 0; i < nbins; i++)
    bins[i] = 0;

  for (i = 0; i < nNumBeads; i++) {

    dx = beadList[i].x - xm[0];
    dy = beadList[i].y - xm[1];
    dz = beadList[i].z - xm[2];

    r = sqrt(dx*dx + dy*dy + dz*dz);
    ibin = (int)(r / dr);
    if (ibin < nbins)
      bins[ibin]++;
  }

  double vol, v1, v2;
  double factor = 4.0*(4.0*atan(1.0))/3.0;
  ofstream ofs;
  ofs.open(filename);
  for (i = 0; i < nbins; i++) {
    ofs << i * dr << " " << bins[i] << " ";
    v1 = (i*dr)*(i*dr)*(i*dr);
    v2 = ((i+1)*dr)*((i+1)*dr)*((i+1)*dr);
    vol = factor * (v2 - v1);
    ofs << (double)bins[i] / vol << "\n";
  }

  ofs.close();
  delete [] bins;
}

// find the coverage percentage
// input: beadList = polymer beads
//        surface contains the protein beads on the outer layer
//        xm is the center of mass (COM) of the protein
//        skin2 is the squared shell radius
//        nbins is the number of bins in azimuthal and polar dims
// output: outlist contains the polymer beads in the shell
double coverage(BeadList& beadList, BeadList& ref,
                double* xm, double skin2, int nbins, int nbeadsperchain, BeadList& outlist)
{
  int i, j, m, ix, iy, idx;
  double dx, dy, dz, rsq, rxy, theta, phi;
  int nNumBeads = beadList.size();
  double pi = 4.0*atan(1.0);

  double* bins = new double [nbins*nbins];
  double dphi = 360.0 / nbins;
//  double dtheta = 180.0 / nbins;
  double dcostheta = 2.0 / nbins; // costheta ranges from -1 to 1
  for (i = 0; i < nbins*nbins; i++)
    bins[i] = 0;

  int nchains = nNumBeads / nbeadsperchain;
  int* nboundsites = new int [nchains];
  for (i = 0; i < nchains; i++) nboundsites[i] = 0;

  vector<int> indices;
	for (i = 0; i < nNumBeads; i++)	{

    // check if the bead is within the bounding sphere

		dx = beadList[i].x - xm[0];
		dy = beadList[i].y - xm[1];
		dz = beadList[i].z - xm[2];
    rsq = dx*dx + dy*dy + dz*dz;
    if (rsq > skin2) continue;

    // check if the bead is close to any bead that belongs to the reference group (i.e. protein)

    int nref = ref.size();
    double tx, ty, tz, trsq, proximity2;
    proximity2 = 1.5*1.5;
    int near_surface = 0;
    for (j = 0; j < nref; j++) {
  		tx = beadList[i].x - ref[j].x;
  		ty = beadList[i].y - ref[j].y;
  		tz = beadList[i].z - ref[j].z;
      trsq = tx*tx + ty*ty + tz*tz;
      if (trsq <= proximity2) {
        near_surface = 1;
        break;
      }
    }
    if (near_surface == 0) continue;

    rxy = sqrt(dx*dx + dy*dy);
    double costheta = dz / sqrt(dz*dz+rxy*rxy);
//    theta = atan2(rxy, dz);  // since rxy >= 0, theta falls into [0:pi]
//    theta = theta * 180.0 / pi;
    phi = atan2(dy, dx);
    if (phi < 0) phi += 2.0*pi;
    phi = phi * 180.0 / pi;

    ix = (int)((phi - 0) / dphi);
//    iy = (int)((theta - 0) / dtheta);
    iy = (int)((costheta + 1.0) / dcostheta);
    if (ix >= 0 && ix < nbins && iy >= 0 && iy < nbins)
      bins[ix*nbins+iy] += 1.0;

//    outlist.push_back(beadList[i]);
    int chain_id = i / nbeadsperchain;
    int found = 0;
    nboundsites[chain_id]++; // increase in the number of bounded beads for chain_id 
    for (int m = 0; m < indices.size(); m++)
      if (chain_id == indices[m]) found = 1;
    if (found == 0) indices.push_back(chain_id);

	}

  sort(indices.begin(), indices.end());
  for (i = 0; i < indices.size(); i++) {
    int m = indices[i];
    for (j = 0; j < nbeadsperchain; j++) {
      outlist.push_back(beadList[m*nbeadsperchain+j]);
    }
  }

  // count the number of bins that have beads

  int nonzero_bins = 0;
  for (i = 0; i < 2*nbins*nbins; i++)
    if (bins[i] > 0) nonzero_bins++;
  
  delete [] bins;

  double nbound_ave = 0;
  int count = 0;
  for (i = 0; i < nchains; i++) {
    if (nboundsites[i] > 0) {
      nbound_ave += nboundsites[i];
      count++;
    }
  }
  nbound_ave /= (double)count;
  printf("Bounded beads per chain: %f\n", nbound_ave);
  printf("Number of attached chains: %d\n", count);

  return (double)nonzero_bins/(double)(2*nbins*nbins);
}

double coverage_geodesic(BeadList& beadList, BeadList& ref,
                double* xm, double skin2, double** bincoords,
                int nbins, double proximity, int nbeadsperchain, BeadList& outlist)
{
  int i, j, m, ix, iy, idx;
  double xtmp, ytmp, ztmp, dx, dy, dz, rsq, rxy, theta, phi;
  int nNumBeads = beadList.size();
  double pi = 4.0*atan(1.0);

  double* bins = new double [nbins];
  for (i = 0; i < nbins; i++) bins[i] = 0;

  int nchains = nNumBeads / nbeadsperchain;
  int* nboundsites = new int [nchains];
  for (i = 0; i < nchains; i++) nboundsites[i] = 0;

  // radius at the geodesic surface
  dx = bincoords[0][0] - xm[0];
  dy = bincoords[0][1] - xm[1];
  dz = bincoords[0][2] - xm[2];
  double rgeosq = dx*dx + dy*dy + dz*dz;

  vector<int> indices;
  vector<Bead> projlist;

	for (i = 0; i < nNumBeads; i++)	{

    // check if the bead is within the bounding sphere
    xtmp = beadList[i].x;
    ytmp = beadList[i].y;
    ztmp = beadList[i].z;

		dx = xtmp - xm[0];
		dy = ytmp - xm[1];
		dz = ztmp - xm[2];
    rsq = dx*dx + dy*dy + dz*dz;
    if (rsq > skin2) continue;

    // check if the bead is close to any bead that belongs to the reference group (i.e. protein)

    int nref = ref.size();
    double tx, ty, tz, trsq, proximity2;
    proximity2 = proximity*proximity;
    int near_surface = 0;
    for (j = 0; j < nref; j++) {
  		tx = xtmp - ref[j].x;
  		ty = ytmp - ref[j].y;
  		tz = ztmp - ref[j].z;
      trsq = tx*tx + ty*ty + tz*tz;
      if (trsq <= proximity2) {
        near_surface = 1;
        break;
      }
    }
    if (near_surface == 0) continue;

    // project the point on the geodesic surface

    double scale = sqrt(rgeosq / rsq);
    dx *= scale;
    dy *= scale;
    dz *= scale;
   
    for (j = 0; j < nbins; j++) {
      tx = (xm[0] + dx) - bincoords[j][0];
      ty = (xm[1] + dy) - bincoords[j][1];
      tz = (xm[2] + dz) - bincoords[j][2];
      trsq = tx*tx + ty*ty + tz*tz;
      if (trsq <= proximity2) {
        bins[j] += 1.0;

        Bead bead;
        bead.x = xm[0] + dx;
        bead.y = xm[1] + dy;
        bead.z = xm[2] + dz;
        projlist.push_back(bead);
      }
    }



    int chain_id = i / nbeadsperchain;
    int found = 0;
    nboundsites[chain_id]++; // increase in the number of bounded beads for chain_id 
    for (int m = 0; m < indices.size(); m++)
      if (chain_id == indices[m]) found = 1;
    if (found == 0) indices.push_back(chain_id);

	}

  sort(indices.begin(), indices.end());
  for (i = 0; i < indices.size(); i++) {
    int m = indices[i];
    for (j = 0; j < nbeadsperchain; j++) {
      outlist.push_back(beadList[m*nbeadsperchain+j]);
    }
  }

  // count the number of bins that have beads

  int nonzero_bins = 0;
  for (i = 0; i < nbins; i++)
    if (bins[i] > 0) nonzero_bins++;
  


  double nbound_ave = 0;
  int count = 0;
  for (i = 0; i < nchains; i++) {
    if (nboundsites[i] > 0) {
      nbound_ave += nboundsites[i];
      count++;
    }
  }
  nbound_ave /= (double)count;
  printf("Bounded beads per chain: %f\n", nbound_ave);
  printf("Number of attached chains: %d\n", count);
  printf("Number of occupied bins: %d\n", nonzero_bins);
/*
  FILE* fp = fopen("projected.txt", "w");
  float xlo = 0;
  float xhi = 2*xm[0];
  float ylo = 0;
  float yhi = 2*xm[1];
  float zlo = 0;
  float zhi = 2*xm[2];

  fprintf(fp, "ITEM: TIMESTEP\n0\n");
  fprintf(fp, "ITEM: NUMBER OF ATOMS\n%d\n", projlist.size());
  fprintf(fp, "ITEM: BOX BOUNDS pp pp pp\n");
  fprintf(fp, "%f %f\n", xlo, xhi);
  fprintf(fp, "%f %f\n", ylo, yhi);
  fprintf(fp, "%f %f\n", zlo, zhi);
  fprintf(fp, "ITEM: ATOMS id type q x y z\n");

  int id = 1;
  for (int i = 0; i < projlist.size(); i++) {
    fprintf(fp, "%d 1 0 %g %g %g\n", id, projlist[i].x, projlist[i].y, projlist[i].z);
    id++;
  }

  fclose(fp);

  fp = fopen("bins.txt", "w");

  fprintf(fp, "ITEM: TIMESTEP\n0\n");
  fprintf(fp, "ITEM: NUMBER OF ATOMS\n%d\n", nbins);
  fprintf(fp, "ITEM: BOX BOUNDS pp pp pp\n");
  fprintf(fp, "%f %f\n", xlo, xhi);
  fprintf(fp, "%f %f\n", ylo, yhi);
  fprintf(fp, "%f %f\n", zlo, zhi);
  fprintf(fp, "ITEM: ATOMS id type q x y z\n");

  id = 1;
  for (int i = 0; i < nbins; i++) {
    if (bins[i] > 0)
      fprintf(fp, "%d 1 0 %g %g %g\n", id, bincoords[i][0],bincoords[i][1], bincoords[i][2]);
    else
      fprintf(fp, "%d 2 0 %g %g %g\n", id, bincoords[i][0],bincoords[i][1], bincoords[i][2]);
    id++;
  }


  fclose(fp);
*/
  delete [] bins;
  delete [] nboundsites;

  return (double)nonzero_bins/(double)(nbins);
}

double surface_pattern(BeadList& beadList, double* xm, int nbins)
{
  int i, j, m, ix, iy, idx;
  double dx, dy, dz, rsq, rxy, theta, phi;
  int nNumBeads = beadList.size();
  double pi = 4.0*atan(1.0);

  double* bins = new double [2*nbins*nbins];
  double dphi = 360.0 / (2*nbins);
  double dtheta = 180.0 / nbins;
  for (i = 0; i < 2*nbins*nbins; i++)
    bins[i] = 0;

	for (i = 0; i < nNumBeads; i++)	{
		dx = beadList[i].x - xm[0];
		dy = beadList[i].y - xm[1];
		dz = beadList[i].z - xm[2];
    rxy = sqrt(dx*dx + dy*dy);
    theta = atan2(rxy, dz);  // since rxy >= 0, theta falls into [0:pi]
    theta = theta * 180.0 / pi;
    phi = atan2(dy, dx);
    if (phi < 0) phi += 2.0*pi;
    phi = phi * 180.0 / pi;

    ix = (int)((phi - 0) / dphi);
    iy = (int)((theta - 0) / dtheta);
    if (ix >= 0 && ix < 2*nbins && iy >= 0 && iy < nbins)
      bins[ix*nbins+iy] += 1.0;

	}

  // count the number of bins that have beads

  int nonzero_bins = 0;
  for (i = 0; i < 2*nbins*nbins; i++)
    if (bins[i] > 0) nonzero_bins++;
  
  delete [] bins;

  return (double)nonzero_bins/(double)(2*nbins*nbins);
}

// pair correlation function between two bead groups
// dr   = bin size
// rmax = range of the histogram
void rdf(BeadList& list1, BeadList& list2, double Lx, double Ly, double Lz,
         double dr, double rmax, char* filename)
{
  int i, j, nbins, n1, n2;
  double xtmp, ytmp, ztmp, dx, dy, dz, rsq;
  double Lx2 = 0.5*Lx;
  double Ly2 = 0.5*Ly;
  double Lz2 = 0.5*Lz;

  n1 = list1.size();
  n2 = list2.size();

  nbins = int(rmax / dr) + 1;
  double* g = new double [nbins];
  for (i = 0; i < nbins; i++) g[i] = 0.0;

  for (i = 0; i < n1; i++) {
    xtmp = list1[i].x;
    ytmp = list1[i].y;
    ztmp = list1[i].z;
    for (int j = 0; j < n2; j++) {
      dx = xtmp - list2[j].x;
      dy = ytmp - list2[j].y;
      dz = ztmp - list2[j].z;
      if (dx <= -Lx2) dx += Lx;
      else if (dx >= Lx2) dx -= Lx;
      if (dy <= -Ly2) dy += Ly;
      else if (dy >= Ly2) dy -= Ly;
      if (dz <= -Lz2) dz += Lz;
      else if (dz >= Lz2) dz -= Lz;
      rsq = dx * dx + dy * dy + dz * dz;
      // exclude the self correlation
      if (rsq < EPSILON) continue;
      unsigned int ibin = (unsigned int)(sqrt(rsq) / dr);
      if (ibin >= 0 && ibin < nbins) 
          g[ibin] += 1.0f;
    }
  }

  // non-overlapping sets
  int npairs = n1 * n2;
  double factor = 4.0/3.0*4.0*atan(1.0)*pow((double)dr,3.0);
  double box_vol = Lx * Ly * Lz;

  ofstream ofs(filename);
  for (i = 0; i < nbins; i++) {
    double dv = factor * (pow((double)(i+1), 3.0) - pow((double)i, 3.0));
    double gr = (double)g[i] / (npairs * dv / box_vol);
    ofs << i * dr << " " << g[i] << " " << gr << "\n";
  }

  ofs.close();
  delete [] g;
}

double squared_end_to_end_distance(const BeadList& beadList, int nbeadsperchain,
                                   double Lx, double Ly, double Lz)
{
  int i, nNumBeads, nchains, startIdx, endIdx;
	double dx, dy, dz, Lx2, Ly2, Lz2, rsq;
  int periodic_wrapped = 0;

  Lx2 = Lx / 2.0;
	Ly2 = Ly / 2.0;
	Lz2 = Lz / 2.0;

	nNumBeads = beadList.size();
  nchains = nNumBeads / nbeadsperchain;
  rsq = 0;
	for (i = 0; i < nchains; i++)	{
    startIdx = i * nbeadsperchain;
    endIdx = startIdx + nbeadsperchain - 1;
    dx = beadList[startIdx].x - beadList[endIdx].x;
		dy = beadList[startIdx].y - beadList[endIdx].y;
		dz = beadList[startIdx].z - beadList[endIdx].z;

   	if (dx > Lx2) dx -= Lx;
		else if (dx < -Lx2) dx += Lx;
		if (dy > Ly2) dy -= Ly;
		else if (dy < -Ly2)	dy += Ly;
		if (dz > Lz2) dz -= Lz;
    else if (dz < -Lz2) dz += Lz;

		rsq += dx * dx + dy * dy + dz * dz;
	}

  rsq /= (double)nchains;
  return rsq;
}

int get_num_contacts(BeadList& list1, BeadList& list2, double Lx, double Ly, double Lz,
         double cutoff)
{
  int i, j, nbins, n1, n2;
  double xtmp, ytmp, ztmp, dx, dy, dz, rsq;
  double Lx2 = 0.5*Lx;
  double Ly2 = 0.5*Ly;
  double Lz2 = 0.5*Lz;
  double cutoff2 = cutoff * cutoff;

  n1 = list1.size();
  n2 = list2.size();

  int n = 0;
  for (i = 0; i < n1; i++) {
    xtmp = list1[i].x;
    ytmp = list1[i].y;
    ztmp = list1[i].z;
    for (int j = 0; j < n2; j++) {
      dx = xtmp - list2[j].x;
      dy = ytmp - list2[j].y;
      dz = ztmp - list2[j].z;
      if (dx <= -Lx2) dx += Lx;
      else if (dx >= Lx2) dx -= Lx;
      if (dy <= -Ly2) dy += Ly;
      else if (dy >= Ly2) dy -= Ly;
      if (dz <= -Lz2) dz += Lz;
      else if (dz >= Lz2) dz -= Lz;
      rsq = dx * dx + dy * dy + dz * dz;

      if (rsq < cutoff2)
        n++;
    }
  }

  return n;
}

// find the correlation between the adsorbed beads of a chain, then averaging over the chains
// beadList = sticky beads on the polymer chains that have adsorbed beads
// nbeadsperchain = 
// ref = adsorption sites on the protein
void correlation_adsorbed(const BeadList& beadList, int nbeadsperchain, int* types, int ntypes, const BeadList& ref,
                          double cutoff, double Lx, double Ly, double Lz, double rmax)
{
  int i, j, nbins, n, n1, n2;
  double xtmp, ytmp, ztmp, dx, dy, dz, rsq;
  double Lx2 = 0.5*Lx;
  double Ly2 = 0.5*Ly;
  double Lz2 = 0.5*Lz;
  double cutoff2 = cutoff * cutoff;

  n1 = beadList.size();
  n2 = ref.size();

  double dr = 0.1;
  nbins = int(rmax / dr);
  int num_chains = n1 / nbeadsperchain;
  double *g = new double [num_chains*nbins];
  for (i = 0; i < num_chains*nbins; i++) g[i] = 0;

  for (i = 0; i < num_chains; i++) {
    int idx = i * nbeadsperchain;
    BeadList adsorbed;
    for (j = 0; j < nbeadsperchain; j++) {
      xtmp = beadList[idx+j].x;
      ytmp = beadList[idx+j].y;
      ztmp = beadList[idx+j].z;

      int included = 0;
      for (int m = 0; m < ntypes; m++) {
        if (beadList[idx+j].type == types[m]) {
          included = 1;
          break;
        }
      }
      if (included == 0) continue;

      for (int m = 0; m < n2; m++) {
        dx = xtmp - ref[m].x;
        dy = ytmp - ref[m].y;
        dz = ztmp - ref[m].z;
        if (dx <= -Lx2) dx += Lx;
        else if (dx >= Lx2) dx -= Lx;
        if (dy <= -Ly2) dy += Ly;
        else if (dy >= Ly2) dy -= Ly;
        if (dz <= -Lz2) dz += Lz;
        else if (dz >= Lz2) dz -= Lz;
        rsq = dx * dx + dy * dy + dz * dz;

        if (rsq < cutoff2) {  // add the bead once
          Bead bead;
          bead.x = xtmp;
          bead.y = ytmp;
          bead.z = ztmp;
          adsorbed.push_back(bead);
          break;
        }
      }
    }

    // calculate g(r) for the adsorbed beads of each chain
    n = adsorbed.size();
    if (n < 2) continue;
    for (int m = 0; m < n-1; m++) {
      xtmp = adsorbed[m].x;
      ytmp = adsorbed[m].y;
      ztmp = adsorbed[m].z;

      for (int l = m+1; l < n; l++) {
        dx = xtmp - adsorbed[l].x;
        dy = ytmp - adsorbed[l].y;
        dz = ztmp - adsorbed[l].z;
        if (dx <= -Lx2) dx += Lx;
        else if (dx >= Lx2) dx -= Lx;
        if (dy <= -Ly2) dy += Ly;
        else if (dy >= Ly2) dy -= Ly;
        if (dz <= -Lz2) dz += Lz;
        else if (dz >= Lz2) dz -= Lz;
        rsq = dx * dx + dy * dy + dz * dz;
        int ibin = (int)(sqrt(rsq) / dr);

        if (ibin >= 0 && ibin < nbins)
          g[i*nbins+ibin] += 1;
      }


    }
  }

  // averaging over g(r)
  double *g_ave = new double [nbins];
  for (i = 0; i < nbins; i++) g_ave[i] = 0;

  for (i = 0; i < num_chains; i++)
    for (j = 0; j < nbins; j++)
      g_ave[j] += g[i*nbins + j];

  for (i = 0; i < nbins; i++) g_ave[i] /= (double)num_chains;
  
  FILE* fp = fopen("adsorbed_gr.txt", "w");
  fprintf(fp, "r g(r)\n");
  for (i = 0; i < nbins; i++) {
    fprintf(fp, "%g %g ", i*dr, g_ave[i]);
    for (j = 0; j < num_chains; j++) 
      fprintf(fp, "%g ", g[j*nbins+i]);

    fprintf(fp, "\n");
 

//  fprintf(fp, "%g %g\n", i*dr, g_ave[i]);
  }

  fclose(fp);

  delete [] g;
  delete [] g_ave;
}

int create_geodesic_bins(double* xm, double radius, double maxEdgeLength, double**& bincoords)
{
  Vec3 center(xm[0], xm[1], xm[2]);
  GeodegicSphere s(center, radius, maxEdgeLength);
  int nvertices = s.triangulate(bincoords);
  return nvertices;
}

int main(int argc, char** argv)
{
  if (argc < 2) {
    printf("Requires: dumpfile [shell_thickness] [nbins]\n");
    return 1;
  }

  int nTotalNumBeads;
  int nbins = 0;
  int ntypes1 = 4;               // polymer beads of 4 types 
  int types1[4] = {1, 2, 3, 4};  // types 1, 2, 3 and 4
  int ntypes2 = 2;               // protein beads of 2 types
  int types2[2] = {5, 6};        // types 5 and 6
  int ntypes3 = 2;               // hydrophobic polymer beads of 2 types 
  int types3[2] = {1, 2};        // types 1, 2
  int ntypes4 = 1;               // protein beads of 1 type
  int types4[1] = {5};           // type 5
  int ntypes5 = 2;               // hydrophilic polymer beads of 2 types 
  int types5[2] = {3, 4};        // types 3 and 4
  int ntypes6 = 1;               // protein beads of 1 type
  int types6[1] = {6};           // types 6

  int nbeadsperchain = 20;

  double Lx, Ly, Lz, shell_thickness = 3.0, shell_radius, skin2;
  BeadList all, protein, polymers, surface, cover;
  BeadList hydrophobic_polymers, hydrophobic_protein;
  BeadList hydrophilic_polymers, hydrophilic_protein;

  // read the dump file, allocate all the beads

  char* filename = new char [1024];
  sprintf(filename, "%s", argv[1]);
  ifstream ifs(filename);
  read_dump(filename, nTotalNumBeads, Lx, Ly, Lz, all);
  ifs.close();

  double dr = 0.1;           
  double rmax = Lx/2.0;

  // shell thickness would be close to the interaction cutoff between surface beads to polymer beads

  if (argc >= 3) shell_thickness = atof(argv[2]);

  if (argc >= 4) nbins = atof(argv[3]);

  // get the bead lists for the protein and polymers from all
  get_beads(all, types1, ntypes1, polymers);
  write_dump(polymers, Lx, Ly, Lz, (char*)"polymers.dump");
  get_beads(all, types2, ntypes2, protein);
  write_dump(protein, Lx, Ly, Lz, (char*)"protein.dump");

  get_beads(polymers, types3, ntypes3, hydrophobic_polymers);
  write_dump(hydrophobic_polymers, Lx, Ly, Lz, (char*)"hydrophobic_polymers.dump");
  get_beads(polymers, types5, ntypes5, hydrophilic_polymers);
  write_dump(hydrophilic_polymers, Lx, Ly, Lz, (char*)"hydrophilic_polymers.dump");

  // compute the radius of gyration (Rg), center of mass of the protein (xm)
  // and the maximum distance from a protein bead to xm (maxrsq)

  double Rg, R2[3], xm[3], maxrsq;
  xm[0] = 0.5*Lx;
  xm[1] = 0.5*Ly;
  xm[2] = 0.5*Lz;
  if (protein.size() > 0) {
    gyration_tensor(protein, Lx, Ly, Lz, R2, Rg, maxrsq, xm);
/*
    printf("Protein number of beads = %d\n"
           "Center of mass          = (%f %f %f)\n"
           "Max distance to COM     = %f\n"
           "Radius of gyration      = %f\n",
           protein.size(), xm[0], xm[1], xm[2], sqrt(maxrsq), Rg);
*/
  }
  double **bincoords = NULL;
  double maxEdgeLength = 2.0;
  nbins = create_geodesic_bins(xm, sqrt(maxrsq), maxEdgeLength, bincoords);


  density_profile(polymers, xm, dr, rmax, "polymers_profile.txt");

  density_profile(hydrophilic_polymers, xm, dr, rmax, "hydrophilic_polymers_profile.txt");

  // estimate nbins for the given value shell_radius so that the angle bin size is not so small

  shell_radius = sqrt(maxrsq) + shell_thickness;
  double pi = 4.0*atan(1.0);
  double surface_area = 4*pi*shell_radius*shell_radius; // surface area at the shell radius
  double sigma = 1.0;                                   // bead diameter (i.e. the unit length)
  double unit_area = pi*sigma*sigma;
  if (nbins == 0)
    nbins = (int)(sqrt(surface_area / unit_area * 0.909 / 2)); // 0.909 highest circle packing fraction
  skin2 = shell_radius * shell_radius;
  double proximity = 1.4;

  double c = 0;
  if (protein.size() > 0) {

    // find the protein surface coverage
//    c = coverage(polymers, protein, xm, skin2, nbins, nbeadsperchain, cover);
    c = coverage_geodesic(polymers, protein, xm, skin2, bincoords, nbins, proximity, nbeadsperchain, cover);
    printf("Number of bins            = %d\n", nbins);
    printf("Surface coverage, \%      = %0.1f\n", c*100.0);
    write_dump(cover, Lx, Ly, Lz, (char*)"cover.dump");

    // find the squared end-to-end distance of the polymer chains that graft the protein
    double R2squared = squared_end_to_end_distance(cover, nbeadsperchain, Lx, Ly, Lz);
    printf("Average squared end-to-end distance     = %0.1f\n", R2squared);

    // find the outer layer of the protein from outside
    // looking for the protein beads that are in contact with the cover polymer group
    double cutoff = 1.2;
    find_contact_group(protein, cover, cutoff, surface);
    write_dump(surface, Lx, Ly, Lz, (char*)"surface.dump");
    rdf(surface, surface, Lx, Ly, Lz, dr, rmax,
      (char*)"contact-with-polymer-rdf.txt");

    get_beads(protein, types4, ntypes4, hydrophobic_protein);
    write_dump(hydrophobic_protein, Lx, Ly, Lz, (char*)"hydrophobic_protein.dump");
    get_beads(protein, types6, ntypes6, hydrophilic_protein);
    write_dump(hydrophilic_protein, Lx, Ly, Lz, (char*)"hydrophilic_protein.dump");

//    rdf(hydrophobic_protein, hydrophobic_protein, Lx, Ly, Lz, dr, rmax,
//      (char*)"protein-patch-rdf.txt");

    rdf(hydrophobic_protein, hydrophobic_polymers, Lx, Ly, Lz, dr, rmax,
      (char*)"hydrophobic-hydrophobic-rdf.txt");

    rdf(hydrophilic_protein, hydrophilic_polymers, Lx, Ly, Lz, dr, rmax,
      (char*)"hydrophilic-hydrophilic-rdf.txt");

    BeadList hydrophobic_protein_surface;
    get_beads(surface, types4, ntypes4, hydrophobic_protein_surface);
    double s = surface_pattern(hydrophobic_protein_surface, xm, nbins);
    printf("Adsorption site coverage, \%: %f\n", s*100.0);

    write_dump(hydrophobic_protein_surface, Lx, Ly, Lz, (char*)"hydrophobic_protein_surface.dump");

    rdf(hydrophobic_protein_surface, hydrophobic_protein_surface, Lx, Ly, Lz, dr, rmax,
      (char*)"protein-patch-rdf.txt");


    int nhp = get_num_contacts(hydrophobic_protein, hydrophobic_polymers, Lx, Ly, Lz, cutoff);
    int npp = get_num_contacts(hydrophobic_polymers, hydrophobic_polymers, Lx, Ly, Lz, cutoff);
    printf("Number of protein-polymer contacts, nhp: %d\n", nhp);
    printf("Number of polymer-polymer contacts, npp: %d\n", npp);

    correlation_adsorbed(cover, nbeadsperchain, types3, ntypes3, hydrophobic_protein, 1.2, Lx, Ly, Lz, 5.0);
  }

  if (bincoords) {
    for (int i = 0; i < nbins; i++)
      delete [] bincoords[i];
    delete [] bincoords;
  }
  delete [] filename;

  return 0;
}

