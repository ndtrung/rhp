
# type 5 = hydrophobic
# type 6 = hydrophilic
proc visualize {molid res} {

	graphics $molid delete all

  # Draw the polymer chains
	set select_atoms [atomselect $molid "type 1 2 3 4"]
	set natoms1 [$select_atoms num]

  set len 20
	set ncopies [expr $natoms1/$len]

  puts "Drawing $ncopies copies"
	if {$natoms1 <= 0} {
		error "No atoms selected"
	}

  color change rgb 200 0.52 0.80 0.98

	for {set i 0} {$i < $ncopies} {incr i}  {

		set startidx [expr $i * $len]
    draw_chain $molid $startidx $len $res
	}

  draw material Glossy

  # Draw the protein
  set select_atoms2 [atomselect $molid "type 5 6"]
	set natoms2 [$select_atoms2 num]

	for {set i 0} {$i < $natoms2} {incr i}  {

    set atom_index1 [expr $natoms1 + $i]
  	set select_tip1 [atomselect $molid "index $atom_index1"]
    set coord1 [lindex [$select_tip1 get {x y z}] 0]
    set type [lindex [$select_tip1 get {type}] 0]
    if {$type == 5} {
#    	graphics $molid color green
    	graphics $molid color red
    }
    if {$type == 6} {
#    	graphics $molid color red
    	graphics $molid color blue
    }
  	graphics $molid sphere $coord1 radius 0.6 resolution $res
	}

}

# types 1-2 hydrophobic
# types 3-4 hydrophilic
proc draw_chain {molid startidx len res} {

  set nbonds [expr $len - 1]
	for {set i 0} {$i < $nbonds} {incr i}  {

    set atom_index1 [expr $startidx + $i]
  	set atom_index2 [expr $startidx + $i + 1]
  	set select_tip1 [atomselect $molid "index $atom_index1"]
  	set select_tip2 [atomselect $molid "index $atom_index2"]

  	set coord1 [lindex [$select_tip1 get {x y z}] 0]
  	set coord2 [lindex [$select_tip2 get {x y z}] 0]
    set type [lindex [$select_tip1 get {type}] 0]

#   	graphics $molid color white
  	graphics $molid cylinder $coord1 $coord2 radius 0.3 resolution $res filled yes
    # orange red
    if {$type == 1} {
    	graphics $molid color orange
    }
    if {$type == 2} {
    	graphics $molid color orange
    }
    # blueish purple (135, 206, 250)
    if {$type == 3} {
    	graphics $molid color 200
    }
    if {$type == 4} {
    	graphics $molid color 200
    }

#  	graphics $molid sphere $coord1 radius 0.5 resolution $res
#  	graphics $molid sphere $coord2 radius 0.5 resolution $res	
  	draw sphere $coord1 radius 0.5 resolution $res
  	draw sphere $coord2 radius 0.5 resolution $res	
  }

}

color Display Background white
mol modstyle 0 0 points


draw material Glossy

axes location off
display cuedensity 0.20000
display cuemode Exp2 
display shadows on
display ambientocclusion on

visualize 0 32

