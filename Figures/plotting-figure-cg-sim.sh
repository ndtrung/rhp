gnuplot << EOF
set terminal postscript eps enhanced color font "Helvetica,60"
set output "figure-cg-sim.eps"

set key top left reverse Left samplen 2.0 font "Helvetica,50"

set style line 1 lt 1 lc rgb "blue" pt 5 lw 6 ps 3
set style line 2 lt 1 lc rgb "red" pt 7 lw 6 ps 3
set style line 3 lt 1 lc rgb "black" pt 11 ps 3 lw 6
set style line 4 lt 1 lc rgb "dark-green" pt 13 ps 3 lw 6
set style line 5 lt 1 lc rgb "orange" pt 15 ps 3 lw 6

W=1.6
H=4.2
set size W,H

set multiplot layout 2,1

#set ylabel "{/Symbol-Oblique G}"
set ylabel "Surface coverage, %" offset 1,0

set mxtics 2
set mytics 2
set xtics scale 4 format "%0.1f"
set ytics scale 4
set bars 2
#set errorbars scale 2

set border lw 4

set yrange [0:100]

set xtics 0.1 scale 4 format "%g"
set xlabel "{/Symbol-Oblique f}_A"
#set xrange [0.45:0.95]
set xrange [0.25:0.75]
set yrange [0:100]

set size W,H/2
set origin 0,0

#set title "(a) {/Symbol-Oblique e}_{pp}/k_BT = 0.8"

# 0.0 if counting only A, 0.2 if including B
s=0.0
plot '../polymer-polymer-0.8/coverage-e2e-eps1.5.txt' u (\$1+s):2 w lp ls 1 pt 4 t "{/Symbol-Oblique e}_{Hh}/k_BT = 1.5",\
     '' u (\$1+s):2:3 w errorbars ls 1 lw 6 notitle,\
     '../polymer-polymer-0.8/coverage-e2e-eps1.5.txt' u (\$1+s):2 w p ls 1 ps 2 lc rgb "white" notitle,\
     '../polymer-polymer-0.8/coverage-e2e-eps1.2.txt' u (\$1+s):2 w lp ls 2 pt 6 t "{/Symbol-Oblique e}_{Hh}/k_BT = 1.2",\
     '' u (\$1+s):2:3 w errorbars ls 2 lw 6 notitle,\
     '../polymer-polymer-0.8/coverage-e2e-eps1.2.txt' u (\$1+s):2 w p ls 2 ps 2 lc rgb "white" notitle,\
     '../polymer-polymer-0.8/coverage-e2e-eps1.0.txt' u (\$1+s):2 w lp ls 3 pt 10 t "{/Symbol-Oblique e}_{Hh}/k_BT = 1.0",\
     '' u (\$1+s):2:3 w errorbars ls 3 lw 6 notitle,\
     '../polymer-polymer-0.8/coverage-e2e-eps1.0.txt' u (\$1+s):2 w p ls 3 ps 2 lc rgb "white" notitle,\
     '../polymer-polymer-0.8/coverage-e2e-eps0.5.txt' u (\$1+s):2 w lp ls 4 pt 12 t "{/Symbol-Oblique e}_{Hh}/k_BT = 0.5",\
     '' u (\$1+s):2:3 w errorbars ls 4 lw 6 notitle,\
     '../polymer-polymer-0.8/coverage-e2e-eps0.5.txt' u (\$1+s):2 w p ls 4 ps 2 lc rgb "white" notitle

#set title "(b) {/Symbol-Oblique f}_A = 0.7"

set size W,H/2
set origin 0,H/2
set xlabel "{/Symbol-Oblique e}_{Hh}/k_BT"
set ylabel "Surface coverage, %"

set xtics 0.5 scale 4 format "%0.1f"
set xrange [0.4:2.6]

plot '../polymer-polymer-0.3/fA-0.5/fA-0.5-output.txt' u 1:2 w lp ls 1 t "{/Symbol-Oblique e}_{hh}/k_BT = 0.3",\
     '../polymer-polymer-0.3/fA-0.5/fA-0.5-output.txt' u 1:2:3 w errorbars ls 1 lw 6 notitle,\
     '../polymer-polymer-0.5/fA-0.5/fA-0.5-output.txt' u 1:2 w lp ls 2 t "{/Symbol-Oblique e}_{hh}/k_BT = 0.5",\
     '../polymer-polymer-0.5/fA-0.5/fA-0.5-output.txt' u 1:2:3 w errorbars ls 2 lw 6 notitle,\
     '../polymer-polymer-0.8/fA-0.5/fA-0.5-output.txt' u 1:2 w lp ls 3 t "{/Symbol-Oblique e}_{hh}/k_BT = 0.8",\
     '../polymer-polymer-0.8/fA-0.5/fA-0.5-output.txt' u 1:2:3 w errorbars ls 3 lw 6  notitle,\
     '../polymer-polymer-1.2/fA-0.5/fA-0.5-output.txt' u 1:2 w lp ls 4 t "{/Symbol-Oblique e}_{hh}/k_BT = 1.2",\
     '../polymer-polymer-1.2/fA-0.5/fA-0.5-output.txt' u 1:2:3 w errorbars ls 4 lw 6  notitle

unset multiplot

EOF
