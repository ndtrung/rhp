
# type 5 = hydrophobic
# type 6 = hydrophilic
proc visualize {molid res} {

	graphics $molid delete all

  # Draw the polymer chains
	set select_atoms [atomselect $molid "type 1 2 3 4"]
	set natoms1 [$select_atoms num]

  set len 20
	set ncopies [expr $natoms1/$len]

  puts "Drawing $ncopies copies"
	if {$natoms1 <= 0} {
		error "No atoms selected"
	}

  draw material Glossy

  # Draw the protein
  set select_atoms2 [atomselect $molid "type 5 6"]
	set natoms2 [$select_atoms2 num]

	for {set i 0} {$i < $natoms2} {incr i}  {

    set atom_index1 [expr $natoms1 + $i]
  	set select_tip1 [atomselect $molid "index $atom_index1"]
    set coord1 [lindex [$select_tip1 get {x y z}] 0]
    set type [lindex [$select_tip1 get {type}] 0]
    if {$type == 5} { 
#    	graphics $molid color green
    	graphics $molid color red
    }
    if {$type == 6} {
#    	graphics $molid color red
    	graphics $molid color blue
    }
  	graphics $molid sphere $coord1 radius 0.5 resolution $res
	}

}

mol delrep 0 top

mol representation VDW 0.40000 32.0000
mol selection {type 5 6}
mol color Name

mol addrep top

color Name 5 red
color Name 6 blue
color Display Background white

draw material Glossy

axes location off
display cuedensity 0.20000
display cuemode Exp2 
display shadows on
display ambientocclusion on
display resetview


