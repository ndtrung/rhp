This repository contains the input scripts to generate
coarse-grained simulation results for the paper.
Also included are the figures in the text and SI,
data and plotting scripts used for generating the figures.

Author: Trung Nguyen (trung.nguyen at northwestern dot edu)

Prerequisites:

- python 2.7+

- LAMMPS (http://lammps.sandia.gov): stable versions 15Sep16 or later

- C++ compiler (e.g. GNU g++)

Below are the input scripts, configuration files and analysis code:

1. generate_random_chains.py	python script to create random copolymers
2. data.protein			coarse-grained model of the HRP protein
3. in.init			LAMMPS input script to mix the polymers with the proteins and equilibrate
4. run_from_restart.in		LAMMPS input script to equilibrate the polymers using 10 annealing cycles
5. surface_coverage.cpp		C++ code to estimate protein surface coverage by polymer beads
6. snapshot-fA-0.5-epp-0.8-ehp-1.5.dump A representative snapshot (LAMMPS dump file)

Procedure:

- Generate the random copolymer chains:	 python generate_random_chains.py 50 0.5

- Output: data-polymers-50.txt

- Run LAMMPS to initialize the system:	$PATH_TO_LMP/lmp_binary -in in.init

- Output: equilibrated.restart

- Run LAMMPS to equilibrate the system and collect data

	$PATH_TO_LMP/lmp_binary -in run_from_restart.in -v restartfile equilibrated.restart \
		-v polymer_polymer 0.8 -v protein_polymer 1.2

- Output 10 equilibrated configurations final.dump.*

- Compile the analysis code surface_coverage.cpp:
  
	g++ -O2 -o find_coverage surface_coverage.cpp

- Compute protein surface coverage:

	./find_coverage final.dump.3.txt 2.0
