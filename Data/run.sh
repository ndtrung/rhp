#!/bin/sh

# This script is to be run within each polymer-polymer-XYZ folder

ANALYSIS_PATH=/home/ndtrung/Data/random-polymers/titan2/analysis

# shell thickness for surface coverage analysis
thickness=2.0

# ten final configurations
start=1
end=10
interval=1

# Iterate through the compositions

for f in 0.3 0.4 0.5 0.6 0.7
do
  cd fA-$f

  rm -rf fA-$f-output.txt

  echo "Processing fA = $f .."
  echo "eps-cross coverage stdev sq-end-to-end stdev" > fA-$f-output.txt
  for epsilon in 0.5 0.8 1.0 1.2 1.5 2.0 2.5
  do
    if [ ! -e eps-$epsilon ]
    then
      continue
    fi

    cd eps-$epsilon

    rm -rf out-cover.txt end-to-end.txt bounded.txt

    touch out-cover.txt
    touch end-to-end.txt
    touch bounded.txt

    for (( t = $start; t <= $end; t+=$interval ))
    do
    #  echo "Processig final.dump.$t"
      if [ -e final.dump.$t ]
      then
        $ANALYSIS_PATH/test_cover3 final.dump.$t $thickness > out.tmp
        grep Surface out.tmp | awk -v t="$t" '{ printf("%g %s\n", t, $5); }' >> out-cover.txt
        grep squared out.tmp | awk -v t="$t" '{ printf("%g %s\n", t, $6); }' >> end-to-end.txt
#        grep Bounded out.tmp | awk -v t="$t" '{ printf("%g %s\n", t, $5); }' >> bounded.txt
        grep attached out.tmp | awk -v t="$t" '{ printf("%g %s\n", t, $5); }' >> bounded.txt

        if [ $t -eq 1 ]
        then
          cp profile.txt tmp.txt
          cp hydrophobic-hydrophobic-rdf.txt tmp2.txt
          cp hydrophilic-hydrophilic-rdf.txt tmp3.txt
        else
          mv profile.txt profile-$t.txt
          paste tmp.txt profile-$t.txt > tmp-last.txt
          mv tmp-last.txt tmp.txt

          mv hydrophobic-hydrophobic-rdf.txt hydrophobic-hydrophobic-rdf-$t.txt
          paste tmp2.txt hydrophobic-hydrophobic-rdf-$t.txt > tmp2-last.txt
          mv tmp2-last.txt tmp2.txt

          mv hydrophilic-hydrophilic-rdf.txt hydrophilic-hydrophilic-rdf-$t.txt
          paste tmp3.txt hydrophilic-hydrophilic-rdf-$t.txt > tmp3-last.txt
          mv tmp3-last.txt tmp3.txt
        fi  
      fi
    done

    mv tmp.txt profile-all.txt
    mv tmp2.txt hydrophobic-hydrophobic-rdf-all.txt
    mv tmp3.txt hydrophilic-hydrophilic-rdf-all.txt

    $ANALYSIS_PATH/sum_across.sh profile-all.txt > profile-ave-eps-$epsilon.txt
    $ANALYSIS_PATH/sum_across.sh hydrophobic-hydrophobic-rdf-all.txt > hydrophobic-hydrophobic-rdf-ave-eps-$epsilon.txt
    $ANALYSIS_PATH/sum_across.sh hydrophilic-hydrophilic-rdf-all.txt > hydrophilic-hydrophilic-rdf-ave-eps-$epsilon.txt

    paste out-cover.txt end-to-end.txt bounded.txt > output.all

    awk -v e="$epsilon" 'BEGIN{sum1=0; sum1sq=0; count=0; sum2=0;sum2sq=0; sum3=0;sum3sq=0;} {
      sum1 += $2; sum1sq += $2*$2;
      sum2 += $4; sum2sq += $4*$4;
      sum3 += $6; sum3sq += $6*$6;
      count++;
    } END{
        printf("%0.1f %g %g %g %g %g %g\n", e, sum1/count, sqrt(sum1sq/count-(sum1/count)**2),
        sum2/count, sqrt(sum2sq/count-(sum2/count)**2),
        sum3/count, sqrt(sum3sq/count-(sum3/count)**2));
    }' output.all >> ../fA-$f-output.txt

    cd ../
  done

  cd ../

done

for epsilon in 0.5 0.8 1.0 1.2 1.5 2.0
do
  echo "fA coverage stdev sq-end-to-end stdev" > coverage-e2e-eps$epsilon.txt
  echo "fA bounded stdev" > bounded-eps$epsilon.txt
  for f in 0.3 0.4 0.5 0.6 0.7
  do
    cd fA-$f

    if [ -e fA-$f-output.txt ]
    then
      awk -v e="$epsilon" -v f="$f" '{
        if ($1 == e) {
          printf("%g %s %s %s %s\n", f, $2, $3, $4, $5);
        }
      }' fA-$f-output.txt >> ../coverage-e2e-eps$epsilon.txt

      awk -v e="$epsilon" -v f="$f" '{
        if ($1 == e) {
          printf("%g %s %s\n", f, $6, $7);
        }
      }' fA-$f-output.txt >> ../bounded-eps$epsilon.txt
    fi

    cd ../
  done
done

if [ $# -eq 1 ]
then
  $ANALYSIS_PATH/plotting-rdf.sh $1
  $ANALYSIS_PATH/plotting-e2e.sh $1
  $ANALYSIS_PATH/plotting-coverage.sh $1
  $ANALYSIS_PATH/plotting-profile.sh $1
  $ANALYSIS_PATH/plotting-bounded.sh $1
fi
