gnuplot << EOF
set terminal postscript eps enhanced color font "Helvetica,50"
set output "thermo.eps"

set key right top reverse Left samplen 2.0 font "Helvetica, 50"

set style line 1 lt 1 lc rgb "blue" pt 5 lw 4 ps 2
set style line 2 lt 2 lc rgb "red" pt 7 lw 4 ps 2
set style line 3 lt 3 lc rgb "black" pt 11 ps 2 lw 4
set style line 4 lt 4 lc rgb "dark-green" pt 13 ps 2 lw 4
set style line 5 lt 5 lc rgb "orange" pt 15 ps 2 lw 4

W=2.2
H=1.8
set size W,H

set border lw 3

set xlabel "t/10^3{/Symbol-Oblique t}"
set ylabel "{/Helvetica-Italic U/N}" rotate by 0
set y2label "{/Helvetica-Italic T}" rotate by 0

#set title "A"

set mxtics 2
set mytics 2
set xtics scale 4 format "%g"
set ytics 0.2 scale 4 format "%0.1f" nomirror
set y2tics 0.2 scale 4 format "%0.1f"

set xrange [0:54]
set yrange [-0.8:0.8]

set y2range [0:1.3]
plot 'thermo-example.txt' u (\$1*0.005/1000):(\$3) w l ls 2 axes x1y1 t "{/Helvetica-Italic U/N}",\
'' u (\$1*0.005/1000):(\$2) w l ls 1 axes x1y2 t "{/Helvetica-Italic T}"

EOF
