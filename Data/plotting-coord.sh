gnuplot << EOF
set terminal postscript eps enhanced color font "Helvetica,50"
set output "coord-0.8-fA-0.5.eps"

set key right top reverse Left samplen 2.0 font "Helvetica, 50"

set style line 1 lt 1 lc rgb "blue" pt 5 lw 4 ps 2
set style line 2 lt 2 lc rgb "red" pt 7 lw 4 ps 2
set style line 3 lt 3 lc rgb "black" pt 11 ps 2 lw 4
set style line 4 lt 4 lc rgb "dark-green" pt 13 ps 2 lw 4
set style line 5 lt 5 lc rgb "orange" pt 15 ps 2 lw 4

W=2.0
H=1.8
set size W,H

set xlabel "t/{/Symbol-Oblique t}"
set ylabel "n_b" rotate by 0

set border lw 4

set mxtics 2
set mytics 2
set xtics scale 4 format "%g"
set ytics 0.2 scale 4 format "%0.1f"
set bars 2
#set errorbars scale 2

#set xrange [0:20]
set yrange [0:1.5]

#set title "{/Symbol-Oblique e}_{polymer-polymer}/k_BT = $1"

plot '../coord-atom/eps-2.0/log.txt' u ((\$1-20900000)*0.005):6 w l ls 1 t "{/Symbol-Oblique e}_{pp}/k_BT = 2.0",\
     '../coord-atom/eps-1.5/log.txt' u ((\$1-22100000)*0.005):6 w l ls 2 t "{/Symbol-Oblique e}_{pp}/k_BT = 1.5",\
     '../coord-atom/eps-1.0/log.txt' u ((\$1-20900000)*0.005):6 w l ls 3 t "{/Symbol-Oblique e}_{pp}/k_BT = 1.0",\
     '../coord-atom/eps-0.5/log.txt' u ((\$1-20900000)*0.005):6 w l ls 4 t "{/Symbol-Oblique e}_{pp}/k_BT = 0.5"

EOF
