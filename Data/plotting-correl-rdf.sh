gnuplot << EOF
set terminal postscript eps enhanced color font "Helvetica,50"
set output "correl-rdf.eps"

set key top right reverse Left samplen 2.0 font "Helvetica,50"

set style line 1 lt 1 lc rgb "blue"       pt 5 lw 4 ps 2
set style line 2 lt 1 lc rgb "red"        pt 7 lw 4 ps 2
set style line 3 lt 1 lc rgb "dark-green" pt 11 lw 4 ps 2 
set style line 4 lt 1 lc rgb "gray"       pt 9 lw 4 ps 2
set style line 5 lt 1 lc rgb "orange"     pt 13 lw 4 ps 2

W=2.0
H=1.8
set size W,H

set xlabel "r/{/Symbol-Oblique s}"
set ylabel "g(r)" rotate by 0

set mxtics 2
set mytics 2
set xtics scale 4 format "%0.1f"
set ytics scale 4 format ""
set bars 2
#set errorbars scale 2

set border lw 4
set xrange [0:20]
set yrange [0:500]

#plot 'polymer-contact-rdf-ehp-1.5-fA-0.7.txt' u 1:2 w lp ls 1 t "{/Symbol-Oblique f}_A = 0.9",\
#     'polymer-contact-rdf-ehp-1.5-fA-0.5.txt' u 1:2 w lp ls 2 t "{/Symbol-Oblique f}_A = 0.7",\
#     'polymer-contact-rdf-ehp-1.5-fA-0.3.txt' u 1:2 w lp ls 3 t "{/Symbol-Oblique f}_A = 0.5",\
#     'protein-patch-rdf-0.5.txt'   u 1:2 w lp ls 4 t "Protein attractive sites"

plot 'polymer-contact-rdf-ehp-1.5-fA-0.7.txt' u 1:2 w lp ls 1 t "{/Symbol-Oblique f}_A = 0.7",\
     'polymer-contact-rdf-ehp-1.5-fA-0.5.txt' u 1:2 w lp ls 2 t "{/Symbol-Oblique f}_A = 0.5",\
     'polymer-contact-rdf-ehp-1.5-fA-0.3.txt' u 1:2 w lp ls 3 t "{/Symbol-Oblique f}_A = 0.3",\
     'protein-patch-rdf-0.5.txt'   u 1:2 w lp ls 4 t "Protein attractive sites"

EOF
