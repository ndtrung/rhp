gnuplot << EOF
set terminal postscript eps enhanced color font "Helvetica,50"
set output "coverage-ratios.eps"

set key top left reverse Left samplen 2.0 font "Helvetica,40"

set style line 1 lt 1 lc rgb "blue"       pt 5 lw 3 ps 3
set style line 2 lt 1 lc rgb "red"        pt 7 lw 3 ps 3
set style line 3 lt 1 lc rgb "dark-green" pt 11 lw 3 ps 3 
set style line 4 lt 1 lc rgb "gray"       pt 9 lw 3 ps 3
set style line 5 lt 1 lc rgb "orange"     pt 13 lw 3 ps 3

W=2.0
H=1.8
set size W,H

set xlabel "{/Symbol-Oblique e}_{hp}/k_BT"
set ylabel "{/Symbol-Oblique G}"  rotate by 0

set mxtics 2
set mytics 2
set xtics scale 4 format "%0.1f"
set ytics scale 4
set bars 2
#set errorbars scale 2

set border lw 4
set xrange [0.4:2.6]
set yrange [0:100]

plot 'ratio-100.txt' u 1:2 w lp ls 1 t "{/Helvetica-Italic N}_c = 100",\
'' u 1:2:3 w errorbars ls 1 notitle,\
'ratio-50.txt' u 1:2 w lp ls 2 t "{/Helvetica-Italic N}_c = 50",\
'' u 1:2:3 w errorbars ls 2 notitle,\
'ratio-12.txt' u 1:2 w lp ls 3 t "{/Helvetica-Italic N}_c = 12",\
'' u 1:2:3 w errorbars ls 3 notitle

EOF
