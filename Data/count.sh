#!/bin/sh

file=$1
type=$2
awk -v type="$type" 'BEGIN{count=0;}{
  if (NR >= 10 && NF == 7) {
    if ($3 == type) count += 1;
  }
} END{printf("%d\n", count);}' $file
