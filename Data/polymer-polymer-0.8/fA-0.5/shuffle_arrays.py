# Remember to make the box dims to be 65.0 

import random
import sys

# Number of chains: 12, 50 or 225
nchains = int(sys.argv[1])

# N = polymerization degree: 20 (sigma = 5.0) or 40 (sigma = 2.5)
# A10B4C5D1 
# A20B8C10D2 
N = 20
N1 = int(0.5*N)
N2 = N1+int(0.2*N)
N3 = N2+int(0.25*N)

array = []
for i in range(1, N+1):
  if i <= N1:
    array.append(1)
  elif i <= N2:
    array.append(2)
  elif i <= N3:
    array.append(3)
  else:
    array.append(4)

# Box bounds
dx = 0.96
dy = 2.0
dz = 2.0

# ny should be divisible by nchains
# ny = 15 for nchains = 225
ny = 40
nz = nchains / ny

xlo = 0
xhi = N+5
ylo = 0
yhi = ny*dy+5
zlo = 0
zhi = nz*dz+5
if xhi < yhi:
  xhi = yhi

dumpfile = open("out.dump",'w')
dumpfile.write("ITEM: TIMESTEP\n0\n")
dumpfile.write("ITEM: NUMBER OF ATOMS\n%d\n" % (N*nchains))
dumpfile.write("ITEM: BOX BOUNDS pp pp pp\n")
dumpfile.write(str(xlo) + " " + str(xhi) + "\n")
dumpfile.write(str(ylo) + " " + str(yhi) + "\n")
dumpfile.write(str(zlo) + " " + str(zhi) + "\n")
dumpfile.write("ITEM: ATOMS id mol type q x y z\n")

datafile = open("data.txt",'w')
datafile.write("LAMMPS data file\n")
datafile.write("%d atoms\n" % (N*nchains))
datafile.write("8 atom types\n%d bonds\n1 bond types\n\n" % ((N-1)*nchains))
datafile.write(str(xlo) + " " + str(xhi) + " xlo xhi\n")
datafile.write(str(ylo) + " " + str(yhi) + " ylo yhi\n")
datafile.write(str(zlo) + " " + str(zhi) + " zlo zhi\n\n")

# Atoms

datafile.write("Atoms\n\n")
for n in range(1,nchains+1):
  random.shuffle(array)
  startid = (n-1)*N
  mol = n
  for i in range(0, N):
    x = xlo + i*dx + 1.0;
    y = ylo + ((n-1) % ny)*dy + 1.0;
    z = zlo + ((n-1) / ny)*dz + 1.0;
    if (array[i] == 1):
      q = -1;
    elif (array[i] == 3):
      q = +1;
    else:
      q = 0;

    s = str(i+1+startid) + " ";
    s += str(mol) + " ";
    s += str(array[i]) + " ";
    s += str(q) + " ";
    s += str(x) + " " + str(y) + " " + str(z) + "\n";
    dumpfile.write(s);
    datafile.write(s);

# Bonds

datafile.write("\nBonds\n\n")
bond_id = 1;
for n in range(1,nchains+1):
  startid = (n-1)*N
  for i in range(1, N):
    id1 = startid+i;
    id2 = id1+1;
    s = str(bond_id) + " 1 " + str(id1) + " " + str(id2) + "\n";
    datafile.write(s);
    bond_id += 1;

dumpfile.close()
datafile.close()

