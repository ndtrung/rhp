gnuplot << EOF
set terminal postscript eps enhanced color font "Helvetica, 50"
set output "surface.eps"

set key out reverse Left samplen 2.0 font "Helvetica, 50"

set style line 1 lt 1 lc rgb "blue" pt 5 lw 4 ps 2
set style line 2 lt 1 lc rgb "red" pt 7 lw 4 ps 2
set style line 3 lt 1 lc rgb "black" pt 11 ps 2 lw 4
set style line 4 lt 1 lc rgb "dark-green" pt 13 ps 2 lw 4
set style line 5 lt 1 lc rgb "orange" pt 15 ps 2 lw 4

H=4.5
W=2.0

set size W,H
set multiplot layout 3,1

set mxtics 2
set mytics 2
set bars 2
#set errorbars scale 2
set border lw 4

set size W,H/3
set origin 0,0
#set title "{/Symbol-Oblique e}_{polymer-polymer}/k_BT = $1"
set title "C" font "Helvetica, 60"
set xlabel "R/{/Symbol-Oblique s}"
set ylabel "{/Symbol-Oblique r}(R), {/Symbol-Oblique s}^{-3}"
set xrange [0:16]
set yrange [0:0.6]
set xtics 2 scale 4 format "%g"
set ytics 0.1 scale 4 format "%0.1f"

plot '../polymer-polymer-0.8/fA-0.5/eps-1.2/profile-ave-eps-1.2.txt' u 1:2 w lp ls 1 t "{/Symbol-Oblique e}_{hp}/k_BT = 1.2",\
     '../polymer-polymer-0.8/fA-0.5/eps-1.0/profile-ave-eps-1.0.txt' u 1:2 w lp ls 2 t "{/Symbol-Oblique e}_{hp}/k_BT = 1.0",\
     '../polymer-polymer-0.8/fA-0.5/eps-0.8/profile-ave-eps-0.8.txt' u 1:2 w lp ls 3 t "{/Symbol-Oblique e}_{hp}/k_BT = 0.8",\
     '../polymer-polymer-0.8/fA-0.5/eps-0.5/profile-ave-eps-0.5.txt' u 1:2 w lp ls 4 t "{/Symbol-Oblique e}_{hp}/k_BT = 0.5"

#set title "{/Symbol-Oblique e}_{polymer-polymer}/k_BT = $1"
set title "B" font "Helvetica, 60"
set size W,H/3
set origin 0,H/3
set xlabel "r/{/Symbol-Oblique s}"
set ylabel "g_{hp}(r)"
set xrange [0:20]
set yrange [0:150]
set xtics 2 scale 4 format "%g"
set ytics 40 scale 4 format ""

plot '../polymer-polymer-0.8/fA-0.5/eps-1.2/hydrophobic-hydrophobic-rdf-ave-eps-1.2.txt' u 1:2 w lp ls 1 t "{/Symbol-Oblique e}_{hp}/k_BT = 1.2",\
     '../polymer-polymer-0.8/fA-0.5/eps-1.2/hydrophobic-hydrophobic-rdf-ave-eps-1.2.txt' u 1:2:3 w errorbars ls 1 notitle,\
     '../polymer-polymer-0.8/fA-0.5/eps-1.0/hydrophobic-hydrophobic-rdf-ave-eps-1.0.txt' u 1:2 w lp ls 2 t "{/Symbol-Oblique e}_{hp}/k_BT = 1.0",\
     '../polymer-polymer-0.8/fA-0.5/eps-1.0/hydrophobic-hydrophobic-rdf-ave-eps-1.0.txt' u 1:2:3 w errorbars ls 2 notitle,\
     '../polymer-polymer-0.8/fA-0.5/eps-0.8/hydrophobic-hydrophobic-rdf-ave-eps-0.8.txt' u 1:2 w lp ls 3 t "{/Symbol-Oblique e}_{hp}/k_BT = 0.8",\
     '../polymer-polymer-0.8/fA-0.5/eps-0.8/hydrophobic-hydrophobic-rdf-ave-eps-0.8.txt' u 1:2:3 w errorbars ls 3 notitle,\
     '../polymer-polymer-0.8/fA-0.5/eps-0.5/hydrophobic-hydrophobic-rdf-ave-eps-0.5.txt' u 1:2 w lp ls 4 t "{/Symbol-Oblique e}_{hp}/k_BT = 0.5",\
     '../polymer-polymer-0.8/fA-0.5/eps-0.5/hydrophobic-hydrophobic-rdf-ave-eps-0.5.txt' u 1:2:3 w errorbars ls 4 notitle

#set title "{/Symbol-Oblique e}_{polymer-polymer}/k_BT = $1"
set title "A" font "Helvetica, 60"
set size W,H/3
set origin 0,2*H/3
set xlabel "{/Symbol-Oblique f}_A"
set ylabel "n_{ads}"

set xrange [0.45:1.0]
set yrange [0:60]
set xtics scale 4 format "%g" ("0.5" 0.52, "0.6" 0.62, "0.7" 0.72, "0.8" 0.82, "0.9" 0.92)
set ytics 10 scale 4 format "%g"

set style fill solid 0.9 border -1
set boxwidth 0.2 relative
plot '../polymer-polymer-0.8/bounded-eps0.5.txt' u (\$1+0.2):2 w boxes ls 1 t "{/Symbol-Oblique e}_{hp}/k_BT = 0.5",\
  '' u (\$1+0.2):2:3 w errorbars lt 1 lc rgb "black" lw 3 notitle,\
  '../polymer-polymer-0.8/bounded-eps0.8.txt' u (\$1+0.2+0.02):2 w boxes ls 2 t "{/Symbol-Oblique e}_{hp}/k_BT = 0.8",\
  '' u (\$1+0.2+0.02):2:3 w errorbars lt 1 lc rgb "black" lw 3 notitle,\
  '../polymer-polymer-0.8/bounded-eps1.0.txt' u (\$1+0.2+0.04):2 w boxes ls 3 t "{/Symbol-Oblique e}_{hp}/k_BT = 1.0",\
  '' u (\$1+0.2+0.04):2:3 w errorbars lt 1 lc rgb "black" lw 3 notitle,\
  '../polymer-polymer-0.8/bounded-eps1.2.txt' u (\$1+0.2+0.06):2 w boxes ls 4 t "{/Symbol-Oblique e}_{hp}/k_BT = 1.2",\
  '' u (\$1+0.2+0.06):2:3 w errorbars lt 1 lc rgb "black" lw 3 notitle


unset multiplot

EOF
