#!/bin/sh

file=$1
col=3
pitch=3

if [ "$#" -ge 2 ]
then
  col=$2
fi

if [ "$#" -ge 3 ]
then
  pitch=$3
fi

#echo "r rho stdev"
awk -v col="$col" -v pitch="$pitch" '{
 if (NR >= 1 && NF > 1) {
  sum = 0; sumsq = 0; count = 0;
  for (i = col; i <= NF; i+=pitch) {
    sum += $i; sumsq += $i * $i;
    count++;
  }
  sum /= count;
  stdev = sqrt(sumsq/count - sum*sum);
  printf("%f %8.4f %8.4f\n", $1, sum, stdev);
 }
}' $file
